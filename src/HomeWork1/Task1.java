package HomeWork1;

/*
В переменную записываем число. Надо вывести на экран сколько в этом
        числе цифр и положительное оно или отрицательное. Например, "это
        однозначное положительное число". Достаточно будет определить, является ли
        число однозначным, "двухзначным или трехзначным и более.
*/

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Task1 {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        /*объявляем переменную которой будем присваивать вводимое целое число*/
        String numS;
        int num;
        int k = 0;


        //создаем бесконечный цикл, чтобы не перезапускать по 500 раз
        while (true) {
            System.out.println("Введите число: ");
            numS = reader.readLine();   //считываем данные с клавиатуры
            try {
                if (numS.equals("ex")) { //в случае ввода "ex" прерываем программу
                    System.out.println("Завершение работы...");
                    break;
                }//если не введено слово "ex" - продолжаем работу
                else num = Integer.parseInt(numS); //переводим введенную строку в тип int
                System.out.println("--------------------");
                System.out.println("Введенное вами число: " + num);


                //объявляется строчная пременная для поредеения длины числа
                String l = "хзсколько";
                String mark = "ох, какой знааак";
                char mk = numS.charAt(0);


                if (num == 0) {
                    l = "одно";
                    mark = "нуль не имеет знака";
                }
                if (num == 0 & numS.length() > 1)
                    System.out.println("Все равно не имеет знака, сколько бы раз не написали и минуса не ставили");

                if (num < 0) mark = "отрицательное";
                if (num > 0) mark = "положительное";

                if (numS.length() <= 11) {
                    if (numS.length() == 1 && num > 0 || numS.length() == 2 && mk == '-') {
                        l = "одно";
                    }

                    if (numS.length() == 2 && num > 0 || numS.length() == 3 && mk == '-') {
                        l = "дву";
                    }
                    if (numS.length() == 3 && num > 0 || numS.length() == 4 && num < 0) {
                        l = "трех";
                    }
                    if (numS.length() == 4 && num > 0 || numS.length() == 5 && num < 0) {
                        l = "четырех";
                    }
                    if (numS.length() == 5 && num > 0 || numS.length() == 6 && num < 0) {
                        l = "пяти";
                    }
                    if (numS.length() == 6 && num > 0 || numS.length() == 7 && num < 0) {
                        l = "шести";
                    }
                    if (numS.length() == 7 && num > 0 || numS.length() == 8 && num < 0) {
                        l = "семи";
                    }

                    if (numS.length() == 8 && num > 0 || numS.length() == 9 && num < 0) {
                        l = "восьми";
                    }
                    if (numS.length() == 9 && num > 0 || numS.length() == 10 && num < 0) {
                        l = "девятии";
                    }
                    if (numS.length() == 10 && num > 0 || numS.length() == 11 && num < 0) {
                        l = "десятии";
                    }
                }
                System.out.println(l + "значное");
                System.out.println(mark);
                System.out.println("--------------------");
            } catch (Exception e) {
                System.out.println("выход за пределы, дофигазначное");
            }
        }

    }
}