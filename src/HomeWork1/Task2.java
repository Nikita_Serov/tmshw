package HomeWork1;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/*Треугольник существует только тогда, когда сумма любых двух его сторон
        больше третьей. Определить существует ли такой треугольник. Дано: a, b, c –
        стороны предполагаемого треугольника. Требуется сравнить длину каждого
        отрезка-стороны с суммой двух других. Если хотя бы в одном случае отрезок
        окажется больше суммы двух других, то треугольника с такими сторонами не
        существует.*/

public class Task2 {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        /*объявляем поля для сторон треугольника*/
        String xS, yS, zS;
        double x, y, z;
        // а так же логическую переменную для отслеживания возможности существования треугольника"да/нет, aka (правда/ложь)"
        boolean triangleIsTrue = false; //переменной типа boolean заранее присвоим значение false (ложь), как в случае, если треугольник не существует

        //считываем данные с клавиатуры
        System.out.println("~Введите стороны треугольника~");
        System.out.println("Сторона х: ");
        xS = reader.readLine();
        System.out.println("Сторона y: ");
        yS = reader.readLine();
        System.out.println("Сторона z: ");
        zS = reader.readLine();
        x = Double.parseDouble(xS);
        y = Double.parseDouble(yS);
        z = Double.parseDouble(zS);

        //проверяем возможность существования треугольника с помощью оператора if
        if (x < (y+z) & y < (x+z) & z < (x+y)) triangleIsTrue = true;

        //выводим ответ в зависимости от результата
        if (triangleIsTrue)
            System.out.println("Такой треугольник существует");
        else System.out.println("Такой треугольник не существует");
    }
}
