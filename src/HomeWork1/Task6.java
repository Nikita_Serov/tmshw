package HomeWork1;

/*Даны 2 числа, выберите большее из них*/

public class Task6 {
    public static void main(String[] args){

        /*  объявляем числа*/
        double num1 = 6;
        double num2 = 86;

      /* С помощью условного оператора выводим большее из них,
      в зависимости от результата выводим в консоль ответ*/
        if (num1>num2)
            System.out.println("Большее из двух чисел - первое, значением "+num1);
        else
            System.out.println("Большее из двух чисел - второе, значением "+num2);
    }
}
