package HomeWork1;

/*    Даны 3 целых числа. Найти количество положительных и отрицательных
    чисел в исходном наборе.*/

public class Task5 { //gittest5
    public static void main(String[] args) {
        /*создаем массив целых чисел*/
        int[] numsArray = new int[3];
        /*  объявляем целые числа, добавляя их в массив*/
        numsArray[0] = 0;
        numsArray[1] = -1;
        numsArray[2] = 15;
        /* вводим счетчик количества положительных чисел из набора заданных*/
        int posNumsCount = 0;
        /* вводим счетчик количества отрицательных чисел из набора заданных*/
        int negNumsCount = 0;

        /*с помощью цикла типа foreach проверяем условие для каждого заданного числа на предмет того,
        с каким знаком число*/
        for (int x : numsArray){
            if (x>0) posNumsCount++;
            if (x<0) negNumsCount++;
        }
        //выводим количество положительных чисел
        System.out.println("Количество положительных чисел: "+ posNumsCount);
        System.out.println("Количество отрицательных чисел: "+ negNumsCount);
    }
}
