package HomeWork1;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/*Дано целое число. Если оно является положительным, то прибавить к нему 1
        Если отрицательным, то вычесть из него 2 Если нулевым, то заменить его на
        10 Вывести полученное число.*/

public class Task3 {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        /*объявляем переменную которой будем присваивать вводимое целое число*/
        String numS;
        int num;

        //создаем бесконечный цикл, чтобы не перезапускать по 500 раз
        while(true){
            System.out.println("Введите число: ");
            numS = reader.readLine();   //считываем данные с клавиатуры

            if(numS.equals("ex")) { //в случае ввода "ex" прерываем программу
                System.out.println("Завершение работы...");
                break;}//если не введено слово "ex" - продолжаем работу
            else num = Integer.parseInt(numS); //переводим введенную строку в тип int
            System.out.println("Введенное вами число: " + num);

            if (num == 0) num = 10; //если введен ноль, то меняем его значение на 10
            else if (num < 0) num -= 2; //иначе, если введенное число меньше нуля - отнимаем от него 2
            else num++; //остается не ноль, и не отрицательное => положительное, прибавляем к нему 1

            System.out.println("Результат: " + num); //выврдим ответ
        }
    }
}
