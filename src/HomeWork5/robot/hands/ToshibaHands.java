package HomeWork5.robot.hands;

public class ToshibaHands   implements IHand {
private int price;

    public ToshibaHands(int price) {
        this.price = price;
    }

    public ToshibaHands() {
    }

    @Override
    public void upHand() {
        System.out.println("Поднимаются руки Toshiba");
    }

    @Override
    public int getPrice() {
        return price;
    }
}
