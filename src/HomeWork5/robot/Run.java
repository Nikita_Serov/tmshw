package HomeWork5.robot;

import HomeWork5.robot.hands.SamsungHands;
import HomeWork5.robot.hands.SonyHands;
import HomeWork5.robot.hands.ToshibaHands;
import HomeWork5.robot.heads.SamsungHead;
import HomeWork5.robot.heads.SonyHead;
import HomeWork5.robot.heads.ToshibaHead;
import HomeWork5.robot.legs.SamsungLegs;
import HomeWork5.robot.legs.SonyLegs;
import HomeWork5.robot.legs.ToshibaLegs;
import org.w3c.dom.ls.LSOutput;

import java.sql.SQLOutput;

public class Run {
    public static void main(String[] args) {

        SonyHead sonyHead = new SonyHead(15);
        SamsungHead samsungHead = new SamsungHead(30);
        ToshibaHead toshibaHead = new ToshibaHead(25);

        SonyHands sonyHands = new SonyHands(20);
        SamsungHands samsungHands = new SamsungHands(10);
        ToshibaHands toshibaHands = new ToshibaHands(15);

        SonyLegs sonyLegs = new SonyLegs(10);
        SamsungLegs samsungLegs = new SamsungLegs(15);
        ToshibaLegs toshibaLegs = new ToshibaLegs(20);

        Robot r1 = new Robot(samsungHead, sonyHands, toshibaLegs);
        Robot r2 = new Robot(toshibaHead, samsungHands, sonyLegs);
        Robot r3 = new Robot(sonyHead, toshibaHands, samsungLegs);


        IRobot[] robots = {r1,r2,r3};
        int maxprice = 0;
        int moreExpenciveRobotNumber = 0;

for (int i = 0; i < robots.length; i++){
    System.out.println("Robot #" + (i+1) + ":");
    robots[i].action();
    System.out.println("Его цена: " + robots[i].getPrice());
    if (robots[i].getPrice() > maxprice){
        maxprice = robots[i].getPrice();
    moreExpenciveRobotNumber = i+1;}
}
        System.out.println("Робот с самой большой ценой (" + maxprice + "): Robot#" + moreExpenciveRobotNumber);
    }
}
