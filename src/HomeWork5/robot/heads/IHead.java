package HomeWork5.robot.heads;

public interface IHead {
    void speek();
    int getPrice();
}
