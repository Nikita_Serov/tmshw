package HomeWork5.DressedHuman;

import HomeWork5.DressedHuman.Jacket.AlaskaJacket;
import HomeWork5.DressedHuman.Jacket.LCWaikikiJacket;
import HomeWork5.DressedHuman.Jacket.VandiniJacket;
import HomeWork5.DressedHuman.Pants.ArmaniPants;
import HomeWork5.DressedHuman.Pants.BershkaPants;
import HomeWork5.DressedHuman.Pants.LCWaikikiPants;
import HomeWork5.DressedHuman.Shoes.EccoShoes;
import HomeWork5.DressedHuman.Shoes.NikeShoes;
import HomeWork5.DressedHuman.Shoes.SkechersShoes;

public class Run {
    public static void main(String[] args) {
        AlaskaJacket alaskaJacket = new AlaskaJacket("Tiger Alaska Jacket",50);
        VandiniJacket vandiniJacket = new VandiniJacket("Leather jacket",150);
        LCWaikikiJacket lcWaikikiJacket = new LCWaikikiJacket("Jeans jacket",20);

        ArmaniPants armaniPants = new ArmaniPants("Armani pants",100);
        BershkaPants bershkaPants = new BershkaPants("Bershka jeans",50);
        LCWaikikiPants lcWaikikiPants = new LCWaikikiPants("LCW shorts",15);

        SkechersShoes skechersShoes = new SkechersShoes("Skechers plain toe",70);
        NikeShoes nikeShoes = new NikeShoes("Nike Roshe run",65);
        EccoShoes eccoShoes = new EccoShoes("Ecco melbourne",75);

        Human h1 = new Human("Anna", alaskaJacket, bershkaPants, skechersShoes);
        Human h2 = new Human("Nikolay", vandiniJacket, armaniPants, eccoShoes);
        Human h3 = new Human("Petrovich", lcWaikikiJacket, lcWaikikiPants, nikeShoes);


        Human[] humans = new Human[]{h1, h2, h3};


        for (Human human : humans) {
            System.out.println("Человек под именем " + human.getName());
            human.getDressed();
            human.unress();
            System.out.println("__________________________________");
        }
    }


}
