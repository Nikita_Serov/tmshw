package HomeWork5.DressedHuman.Pants;

public class BershkaPants implements Pants{
    private String name;
    private int price;

    public BershkaPants(String name, int price) {
        this.name = name;
        this.price = price;
    }
    public int getPrice() {
        return price;
    }
    @Override
    public String putOn() {
        return name + " надеты";
    }

    @Override
    public String takeOff() {
        return name + " сняты";
    }
}
