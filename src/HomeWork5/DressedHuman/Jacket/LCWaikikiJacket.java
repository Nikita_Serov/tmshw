package HomeWork5.DressedHuman.Jacket;

public class LCWaikikiJacket implements Jacket {
    private String name;
    private int price;

    public LCWaikikiJacket(String name, int price) {
        this.name = name;
        this.price = price;
    }
    public int getPrice() {
        return price;
    }
    @Override
    public String putOn() {
        return name + " надета";
    }

    @Override
    public String takeOff() {
        return name + " снята";
    }
}
