package HomeWork5.DressedHuman.Shoes;

public class SkechersShoes implements Shoes {
    private String name;
    private int price;

    public SkechersShoes(String name, int price) {
        this.name = name;
        this.price = price;
    }
    public int getPrice() {
        return price;
    }
    @Override
    public String putOn() {
        return name + " обуты";
    }

    @Override
    public String takeOff() {
        return name + " сняты";
    }
}
