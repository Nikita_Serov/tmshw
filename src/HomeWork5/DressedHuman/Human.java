package HomeWork5.DressedHuman;

import HomeWork5.DressedHuman.Jacket.Jacket;
import HomeWork5.DressedHuman.Pants.Pants;
import HomeWork5.DressedHuman.Shoes.Shoes;

public class Human implements isHuman {
    private String name;
    private Jacket jacket;
    private Pants pants;
    private Shoes shoes;


    public Human(String name, Jacket jacket, Pants pants, Shoes shoes) {
        this.name = name;
        this.jacket = jacket;
        this.pants = pants;
        this.shoes = shoes;
    }

    public String getName() {
        return name;
    }


    @Override
    public void getDressed() {
        System.out.println("Персона " + getName() + " одевается:");
        System.out.println(jacket.putOn() + ", " + pants.putOn() + ", " + shoes.putOn()+'.');
    }
@Override
    public void unress() {
        System.out.println("Персона " + getName() + " раздевается:");
        System.out.println(jacket.takeOff()+", "+pants.takeOff()+", "+shoes.takeOff()+'.');
    }


}
