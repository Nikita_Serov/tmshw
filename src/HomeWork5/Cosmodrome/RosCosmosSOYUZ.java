package HomeWork5.Cosmodrome;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Scanner;

public class RosCosmosSOYUZ implements IStart {
    @Override
    public boolean prelaunchCheck() {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Перед запуском необходимо освятить ракету! Выберите сумму в рублях наличными, которую вы заплатите батюшке:");
        int churchCash;
        while (true) {
            try {
                String ch = reader.readLine();
                churchCash = Integer.parseInt(ch);
                break;
            } catch (Exception ex) {
                System.out.println("Ага-ага, ничего слущать не хочу. Наличными, наличными...Так сколько?");
               continue;
            }
        }
        if (churchCash < 100000) {
            System.out.println("Вы не удовлетворили святого отца грешник.");
            return false;
        } else
            System.out.println("Свят-свят!");
        return true;
    }

    @Override
    public void engineLaunch() {
        System.out.println("Подкинули дров в топку! Русские вперед!");
    }

    @Override
    public void start() {
        System.out.println("Дмитрий олегович Рогозин! Батут запущен!");
    }

}
