package HomeWork5.Cosmodrome;

import java.util.Random;

public class Shuttle implements IStart {


    @Override
    public boolean prelaunchCheck() {
        Random r = new Random();
        int randomlaunch = r.nextInt(10);
        System.out.println("randomlaunch = " + randomlaunch);
        return randomlaunch > 3;
    }

    @Override
    public void engineLaunch() {
        System.out.println("Engine is launched, all systems is normal...");
    }

    @Override
    public void start() {
        System.out.println("Shuttle start!");
    }
}
