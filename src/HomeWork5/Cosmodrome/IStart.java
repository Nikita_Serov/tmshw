package HomeWork5.Cosmodrome;

public interface IStart {
    boolean prelaunchCheck();
    void engineLaunch();
    void start();

}
