package HomeWork5.Cosmodrome;
import com.sun.xml.internal.ws.api.model.wsdl.WSDLOutput;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;


public class Cosmodrome {


    public static void main(String[] args) throws IOException {
        IStart start = null;

        NASA nasa = new NASA();
        Shuttle shuttle = new Shuttle();
        RosCosmosSOYUZ soyuz = new RosCosmosSOYUZ();

        System.out.println("Choose you ship:");
        System.out.println("input 1 to NASA");
        System.out.println("input 2 to RosCosmos Soyuz comrade");
        System.out.println("input 3 to simple shuttle");
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String ch = reader.readLine();
        int choise = 0;
        try {
             choise = Integer.parseInt(ch);
        }catch (NumberFormatException exception){
            System.out.println("No-no-no! only 3 numbers!");
            ch = reader.readLine();
        }
        if (choise != 1 & choise != 2 & choise != 3) {
            System.out.println("No-no-no! only 3 numbers!");
        }
        if (choise == 1){
            System.out.println("You choise is NASA starship");
            start = nasa;}
        if (choise == 2){
            System.out.println("Ты выбрал наш скрепный Союз товрищ!");
            start = soyuz;}
        if (choise == 3){
            System.out.println("You choise is NASA starship");
            start = shuttle;}

        launch(start);
    }

    static void launch(IStart start) {
        if (!start.prelaunchCheck())
            System.out.println("Launch failure");
        else {
            start.engineLaunch();
            for (int i = 10; i > 0; i--)
                System.out.println(i);
            start.start();
        }
    }
}
