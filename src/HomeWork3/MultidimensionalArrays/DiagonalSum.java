package HomeWork3.MultidimensionalArrays;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Random;

/*Создаём квадратную матрицу, размер вводим с клавиатуры. Заполняем
        случайными числами в диапазоне от 0 до 50 И выводим на консоль(в виде
        матрицы).
        Почитать сумму четных элементов стоящих на главной диагонали.*/
public class DiagonalSum {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Введите желаемую величину стороны матрицы:");
        String s = reader.readLine();
        int n = Integer.parseInt(s);
        int sum = 0;
        Random r = new Random();
        int[][] darr = new int[n][n];
        System.out.println("_________________________________");
        //цикл заполнения массива и его вывода
        for (int i = 0; i < n; i++) {
            for (int k = 0; k < darr.length; k++) {
                darr[i][k] = r.nextInt(50);
            }
            System.out.println(Arrays.toString(darr[i]));
        }

        //цикл работы с главной диагональю
        for (int i = 0; i < n; i++) {
            if (darr[i][i] % 2 == 0) sum += darr[i][i];
        }
        System.out.println("_________________________________");
        System.out.print("Сумма четных чисел на главной диагонали матрицы " + n + "x" + n + " равна: " + sum);
    }
}
