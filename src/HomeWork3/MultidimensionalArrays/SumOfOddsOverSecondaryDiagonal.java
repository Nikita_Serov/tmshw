package HomeWork3.MultidimensionalArrays;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Random;

/*4)Посчитать сумму четных элементов стоящих над побочной диагональю (не
        включительно)*/
public class SumOfOddsOverSecondaryDiagonal {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Введите желаемую величину стороны матрицы:");
        String s = reader.readLine();
        int n = Integer.parseInt(s);
        long sum = 0;
        Random r = new Random();
        int[][] darr = new int[n][n];
        System.out.println("_________________________________");
        //цикл заполнения массива и его вывода
        for (int i = 0; i < n; i++) {
            for (int k = 0; k < darr.length; k++) {
              darr[i][k] = r.nextInt(50);
            }
            System.out.println(Arrays.toString(darr[i]));
        }

        //цикл работы с побочной диагональю
        for (int i = 0; i <n; i++) { // (1) итерации для строк
            for (int k = 1; k < n; k++) { // (2) итерации для столбцов (содного т.к. не засчитываем первый)
                if ((i + k ) < n) { // (3) проверка индекса, исключающая выход за пределы индекса строки
              if (darr[i][i + k] % 2 == 0) { //проверка на четность
                        sum += darr[i][i + k];} // + к счетчику четных чисел
                    } else break; // (4) если индекс выходит за пределы строки, то прерывается цикл *(2)
                }
            }
        System.out.println("_________________________________");
        System.out.println("Сумма четных элементов над побочной диагональю матрицы: " + sum);
    }
}
