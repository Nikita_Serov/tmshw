package HomeWork3.MultidimensionalArrays;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Random;
/*5)Транспонировать матрицу(1 столбец станет 1-й строкой, 2-й столбец - 2-й
        строкой и т. д.)*/
public class Transposition {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Введите желаемую величину стороны матрицы:");
        String s = reader.readLine();
        int n = Integer.parseInt(s);
        Random r = new Random();
        int[][] darr = new int[n][n];

        System.out.println("_________________________________");

        //цикл заполнения массива и его вывода
        for (int i = 0; i < n; i++) {
            for (int k = 0; k < darr.length; k++) {
                darr[i][k] = r.nextInt(50);
            }
            System.out.println(Arrays.toString(darr[i]));
        }

        System.out.println("_________________________________");

        for (int j = 0; j < n; j++) {
            for (int l = j + 1; l < n; l++) {
                int temporary = darr[j][l];
                darr[j][l] = darr[l][j];
                darr[l][j] = temporary;
            }
        }
        for (int i = 0; i < n; i++) {
            System.out.println(Arrays.toString(darr[i]));
        }
    }
}
