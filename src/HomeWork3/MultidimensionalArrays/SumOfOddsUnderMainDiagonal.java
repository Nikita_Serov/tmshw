package HomeWork3.MultidimensionalArrays;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Random;
/*2)Вывести нечетные элементы находящиеся под главной
        диагональю(включительно).*/
public class SumOfOddsUnderMainDiagonal {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Введите желаемую величину стороны матрицы:");
        String s = reader.readLine();
        int n = Integer.parseInt(s);
        int sum = 0;
        Random r = new Random();
        int[][] darr = new int[n][n];
        System.out.println("_________________________________");
        //цикл заполнения массива и его вывода
        for (int i = 0; i < n; i++) {
            for (int k = 0; k < darr.length; k++) {
                darr[i][k] = r.nextInt(50);
            }
            System.out.println(Arrays.toString(darr[i]));
        }

        //цикл работы с главной диагональю
        for (int i = 0; i < n; i++) { // (1) итерации для строк
            for (int k = 0; k < n; k++) { // (2) итерации для столбцов
                if ((i + k) < n) { // (3) проверка индекса, исключающая выход за пределы индекса строки
                    if(darr[i+k][i]%2!=0){ //проверка на нечетность
                    sum +=darr[i + k][i];} // + к счетчику нечетных чисел
                }else break; // (4) если индекс выходит за пределы строки, то прерывается цикл *(2)
            }
        }
        System.out.println("_________________________________");
        System.out.print("Сумма нечетных чисел под главной диагоналю матрицы " + n + "x" + n + " включительно равна: " + sum);
    }
}
