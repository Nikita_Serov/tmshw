package HomeWork3.MultidimensionalArrays;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Random;

//3)Проверить произведение элементов какой диагонали больше.
public class WhichSumOfAmountsIsMore {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Введите желаемую величину стороны матрицы:");
        String s = reader.readLine(); //читываем строку
        int n = Integer.parseInt(s); //преобразуем строку в тип int
        long pr1 = 1; //переменная произведения чисел главной диагонали
        long pr2 = 1; // побочной диагонали
        Random r = new Random();
        int[][] darr = new int[n][n]; //массив

        System.out.println("_________________________________");
        //цикл
        for (int i = 0; i < n; i++) {
            for (int k = 0; k < darr.length; k++) {
                darr[i][k] = r.nextInt(50);
            }
            System.out.println(Arrays.toString(darr[i]));
            pr1 *= darr[i][i];
            pr2 *= darr[i][(n - 1) - i];
        }
        System.out.println("_________________________________");
        //System.out.println(pr1); //вывод произведения главной диагонали
        //System.out.println(pr2); //произведение побочной
        if (pr1 != pr2) {
            if (pr1 > pr2) System.out.println("Произведение элементов главной диагонали больше");
            else System.out.println("Произведение элементов побочной диагонали больше");
        } else System.out.println("Произведения элементов обеих диагоналей равны");
    }
}
