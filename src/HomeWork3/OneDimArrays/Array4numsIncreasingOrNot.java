package HomeWork3.OneDimArrays;

import java.util.Arrays;
import java.util.Random;

/*6)Создайте массив из 4 случайных целых чисел из отрезка [0;10], выведите его
        на экран в строку. Определить и вывести на экран сообщение о том, является ли
        массив строго возрастающей последовательностью.*/
//6) (Доп.условие) Проверить, различны ли все элементы массива.
public class Array4numsIncreasingOrNot {
    public static void main(String[] args) {
        Random r = new Random();
        int[] arr = new int[4];
        boolean difference = false, isIncreasing = true; //переменная отслеживающая возрастание

        //заполняем массив рандомными згачениями:
        for (int i = 0; i < arr.length; i++) {

            arr[i] = r.nextInt(10);
            // 2 последующие строчки для проверки (раскоментировать одну из строк для проверки осн- и доп- заданий):
            // arr[i] = i; // осн-
            // arr[i] = 7;// доп-
        }
        //вывод массива в строку
        System.out.println(Arrays.toString(arr));

        //цикл с проверкой услови для каждого элемента массива
        for (int k = 1; k < arr.length; k++) {

            if (arr[k - 1] >= arr[k]) {
                isIncreasing = false;
                break;
            }
        }

        /*
        --------------------------------(Доп.условие)--------------------------------
         */

        for (int k = 1; k < arr.length; k++) {
            if (arr[k - 1] == arr[k]) {
                difference = true;
                break;
            }
        }

        //вывод результата условия возрастания
        if (isIncreasing)
            System.out.println("Массив является строго возрастающей последовательностью");
        else
            System.out.println("Массив не является строго возрастающей последовательностью");

        //вывод результата условия равенства элементов
        System.out.print("(Доп.условие) ");
        if (difference)
            System.out.println("Все числа в массиве одинаковы");
        else
            System.out.println("Все числа в массиве не одинаковы");


    }
}
