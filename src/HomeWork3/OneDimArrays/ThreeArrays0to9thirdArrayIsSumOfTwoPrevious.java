package HomeWork3.OneDimArrays;

import java.util.Arrays;
import java.util.Random;

/*8)Создайте два массива из 10 целых случайных чисел из отрезка [0;9] и третий
        массив из 10 действительных чисел. Каждый элемент с i-ым индексом третьего
        массива должен равняться отношению элемента из первого массива с i-ым
        индексом к элементу из второго массива с i-ым индексом. Вывести все три
        массива на экран (каждый на отдельной строке), затем вывести количество
        целых элементов в третьем массиве.*/
/*8) (Доп.условие) Найти второй по величине (т.е. следующий по величине за максимальным)
        элемент в массиве.*/
public class ThreeArrays0to9thirdArrayIsSumOfTwoPrevious {
    public static void main(String[] args) {
        Random r = new Random();
        //инициализируем массивы
        int[] arr1 = new int[10];
        int[] arr2 = new int[10];
        double[] arr3 = new double[10];
        int count = 0; //счетчик кол-ва целых эл-в


        //заполняем массивы рандомными згачениями:
        for (int i = 0; i < arr1.length; i++) {
            arr1[i] = r.nextInt(9);
            arr2[i] = r.nextInt(9);
            arr3[i] = (double) arr1[i] / arr2[i]; //приводим к double, чтобы не потерять знаки после запятой при неявном приведении типов
            if ((arr3[i] % 1) == 0) //проверка является ли i-й элемент 3 массива целым числом
                count++;
        }

  /*
        --------------------------------(Доп.условие)--------------------------------

         для решения доп.условия я беру 1-й из масивов*/
        int max1 = 0;
        int max2 = 0;
        for (int value : arr1) {
            if (value > max1) {
                max2 = max1;
                max1 = value;
            }
            if (value > max2 && max1 > value)
                max2 = value;
        }

        System.out.println(Arrays.toString(arr1));
        System.out.println(Arrays.toString(arr2));
        System.out.println(Arrays.toString(arr3));
        System.out.println("Количество целых элементов в третьем массиве: " + count);
        System.out.println("___________________________");
        System.out.println("(Доп.условие)");
        System.out.println("Максимальное число в первом массиве: " + max1);
        System.out.println("Следующее за максиамльным число в первом массиве: " + max2);
    }
}
