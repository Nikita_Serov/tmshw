package HomeWork3.OneDimArrays;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Random;

/*7)Создайте массив из 12 случайных целых чисел из отрезка [0;15]. Определите
        какой элемент является в этом массиве максимальным и сообщите индекс его
        последнего вхождения в массив.*/
//7) (Доп.условие) Подсчитать, сколько раз встречается элемент с заданным значением.
public class Array12NumsMaxNumberAndHisIndex {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Введите число для доп условия (до 15)");
        String s = reader.readLine();   // ввод числа для доп условия
        int n = Integer.parseInt(s);    // преобразование его в строку
        int count = 0;  // счетчик для количества пэлементов в массиве, равных введенному числу

        Random r = new Random();
        int[] arr = new int[12]; //инициализируем массив
        int max; //переменная хранения макс.значения
        int maxInd = 0; //переменная хранения индекса макс.значения

        //заполняем массив рандомными згачениями:
        for (int i = 0; i < arr.length; i++) {
            arr[i] = r.nextInt(15);
             /*
        --------------------------------(Доп.условие)--------------------------------
         */
            if (arr[i] == n) count++;
        }

        max = arr[0];//изначально принимаем нулевой эл-т как максимальный
        //ищем максимальное значение, проходя по каждому эл-ту массива
        for (int k = 1; k < arr.length; k++) {
            if (arr[k] >= max) {
                max = arr[k];
                maxInd = k;
            }
        }
        System.out.println(Arrays.toString(arr));
        System.out.println("Максимальный элемент массива: " + max);
        System.out.println("Индекс последнего вхождения в массив максимального элемента: " + maxInd);
        System.out.println("(Доп.условие) количество элементов равных (" + n + "): " + count);
    }

}

