package HomeWork3.OneDimArrays;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Random;

/*9)Пользователь должен указать с клавиатуры положительное число, а
        программа должна создать массив указанного размера из случайных целых
        чисел из [0;15] и вывести его на экран в строку. После этого программа должна
        определить и сообщить пользователю о том, сумма какой половины массива
        больше: левой или правой, либо сообщить, что эти суммы модулей равны. Если
        пользователь введёт неподходящее число, то выдать соответствующее
        сообщение*/
//(Доп.условие) 9) Найти наименьший элемент среди элементов с четными индексами массива
public class InputFromKeyboardAndComparisonOfTwoHalfs {
    public static void main(String[] args) throws IOException {
        Random r = new Random();
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        //счетчики сумм половин
        int half1 = 0;
        int half2 = 0;
        int bound = 15; // максимальное случайное число, записываемое в ячейку
        int middle; //переменная для определения середины
        int[] arr; // переменная массива
        int n; // количество элементов в массиве


        //цикл для ввода и проверки желаемого числа элементов в массиве
        while (true) {
            System.out.println("Введите желаемый размер массива: ");
            String s = reader.readLine(); //считывание строки с клавиатуры
            n = Integer.parseInt(s);  //преобразование строки в тип int
            if (n <= 0) //проверка на ввод допустимого числа
                System.out.println("Неподходящее значение");
            else break;
        }


        arr = new int[n]; //инициализация массива размером в введенное число
        middle = n / 2; //определение индекса середины

        for (int i = 0; i < arr.length; i++) {
            arr[i] = r.nextInt(bound); //заполнение рандомными числами
            if (n % 2 == 0) {
                if (i < middle) half1 += arr[i]; //если слева от середины - прибавляем к счетчику первой части
                else half2 += arr[i]; //если справа - правой части
            } else if (i != n / 2) { //if на случай если введенное n является нечетным
                if (i < middle)
                    half1 += arr[i]; // таким образом, если введенное n является нечетным - игнорируем средний элемент
                else half2 += arr[i];
            }
        }

         /*
        --------------------------------(Доп.условие)--------------------------------
         */

        int min = bound; // изначально устанавливаем мимнимум как возможное максимальное числ элемента массва
        // проходим по всем эл-там массива
        for (int i = 0; i < arr.length; i++) {
            if (i % 2 != 0) { // если номер элемента массива нечетный, тогда совершаем действие - иначе игнорируем
                // System.out.println(arr[i]); // для отладки
                if (arr[i] < min) min = arr[i];
            }
        }

        System.out.println(Arrays.toString(arr)); //вывод массива
        if (half1 == half2) System.out.println("Сумма чисел левой и правой стороны массива равны");
        if (half1 > half2) System.out.println("Сумма чисел левой стороны больше, чем правой");
        if (half1 < half2) System.out.println("Сумма числе правой стороны больше, чем левой");
        System.out.println("(Доп.условие) минимальный элемент массива с нечетным индексом равен: " + min);
        //заккоментированные строки для отладки:
        //System.out.println(middle);
        //System.out.println(half1);
        //System.out.println(half2);
    }
}

