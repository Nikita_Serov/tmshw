package HomeWork3.OneDimArrays;

import java.util.Arrays;

/*2)Создайте массив из всех нечётных чисел от 1 до 99, выведите его на экран в
        строку, а затем этот же массив выведите на экран тоже в строку, но в обратном
        порядке (99 97 95 93 … 7 5 3 1).*/
// 2) (Доп.условие) Найти среднее арифметическое элементов с нечетными номерами.
public class From1to99andBackward {
    public static void main(String[] args) {
        int[] arr = new int[50];
        int[] arr2 = new int[arr.length];
        int i = 1;
        double sum = 0;
        int count = 0;
        for (int n = 0; n < arr.length; n++) {
            arr[n] = i;
            arr2[arr.length - 1 - n] = i;
            i += 2;

             /*
        --------------------------------(Доп.условие)--------------------------------
         */
            //if для дополнительного условия (определение элементов с нечетными номерами)
            if (n % 2 != 0) {
                sum += arr[n];
                count++;
            }
        }
        System.out.println(Arrays.toString(arr));
        System.out.println(Arrays.toString(arr2));
        System.out.println("(Доп. условие) Среднее арифметическое всех элементов массива с нечетными номерами: " + sum / count);
        System.out.println("________________________________");
    }

}
