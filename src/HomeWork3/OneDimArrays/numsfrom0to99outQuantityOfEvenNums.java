package HomeWork3.OneDimArrays;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Random;

/*3)Создайте массив из 15 случайных целых чисел из отрезка [0; 99]. Выведите
        массив на экран. Подсчитайте сколько в массиве чётных элементов и выведете
        это количество на экран на отдельной строке.*/
/*3) (Доп.условие) Найти средне арифметическое элементов массива, превосходящих некоторое
число С.*/
public class numsfrom0to99outQuantityOfEvenNums {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Введите число 'c'");
        //ввод числа 'с' для доп. условия
        String s = reader.readLine();
        int c = Integer.parseInt(s);
        Random r = new Random();
        int[] arr = new int[15]; //массив
        double sum = 0;
        int countAdv = 0, count = 0;            //кол-во четных
        for (int i = 0; i < arr.length; i++) {
            arr[i] = r.nextInt(99);
            if (arr[i] % 2 == 0) count++; //счетчик четных чисел

             /*
        --------------------------------(Доп.условие)--------------------------------
         */

            if (arr[i] > c) {
                countAdv++;
                sum += arr[i];
            }
        }
        System.out.println("Массив из 15 сучайных чисел от 0 до 99:");
        System.out.println(Arrays.toString(arr));
        System.out.println("Количество четных чисел в данном массиве = " + count);
        System.out.println("(Доп. условие) Среднее арифметическое элементов массива. значением больше переменной 'c' (" + c + ") равно: " + sum / countAdv);
    }
}
