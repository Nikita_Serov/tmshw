package BlackJack;
import BlackJack.Players.Croupier;
import BlackJack.Players.Player;
import BlackJack.Players.PullOfPlayers;
import java.util.ArrayList;
import java.util.Optional;

public class Game {

    public static void main(String[] args) {
        PullOfPlayers pullOfPlayers = new PullOfPlayers();
        Croupier croupier = new Croupier();
        pullOfPlayers.howManyPlayersWantToPlay();
        pullOfPlayers.getTable().add(croupier);
        croupier.fillingTheDeck(croupier.getDeck());
        croupier.shuffleTheDeck(croupier.getDeck());
        gameProcess(pullOfPlayers.getTable(), croupier);

    }

    public static void gameProcess(ArrayList<Player> table, Croupier croupier) {
        table.forEach(p -> p.takeCard(croupier.getDeck())); // all take theirs first cards

        while (true) { //cycle for game
            boolean isAllEnough = table.stream().allMatch(Player::isEnough); // check for is'nt all players pass their's turns
            if (!isAllEnough) {
                System.out.println(ConsoleColors.ANSI_PURPLE
                        + "Not all players fold, play continues..." + ConsoleColors.ANSI_RESET);
                table.forEach(p -> {
                    if (!p.isLose()) {
                        System.out.println(ConsoleColors.ANSI_CYAN
                                + "-----TURN OF PLAYER: " + p.getName() + "-----" + ConsoleColors.ANSI_RESET);
                        p.turn(croupier.getDeck());
                    }
                });
            } else {
                System.out.println(ConsoleColors.ANSI_PURPLE
                        + "All players fold, result of the game:" + ConsoleColors.ANSI_RESET);
                break;
            }
        }
        printResult(table, croupier);
    }

    public static void printResult(ArrayList<Player> table, Player croupier) {
        table.forEach(p -> System.out.println(ConsoleColors.ANSI_CYAN + p.getName()
                + ConsoleColors.ANSI_RESET + " " + p.toString()));
        ArrayList<Player> listOfWinners = new ArrayList<>();

        if (croupier.isLose()) {
            table.forEach(p -> {
                if (!p.isLose())
                    listOfWinners.add(p);

            });
        } else {
            table.forEach(p -> {
                if (!p.isLose() && p.getCountOfPoints() > croupier.getCountOfPoints())
                    listOfWinners.add(p);
            });
        }

        if (!listOfWinners.isEmpty()) {
            System.out.println(ConsoleColors.ANSI_RED
                    + "Players whose bets are played:" + ConsoleColors.ANSI_RESET);
            listOfWinners.forEach(p -> System.out.println(p.getName()
                    + " points: " + p.getCountOfPoints()));

            Optional<Player> winner = listOfWinners.stream().max(Player::compare);
            winner.ifPresent(player -> System.out.println(
                    ConsoleColors.ANSI_PURPLE
                            + "Absolut winner:" + player.getName() + ConsoleColors.ANSI_RESET));
        } else
            System.out.println(ConsoleColors.ANSI_RED + "No winners..." + ConsoleColors.ANSI_RESET);


        croupier.getHand().forEach(c -> System.out.println(ConsoleColors.ANSI_RED + "Croupier Hashcode of turn " +
                (croupier.getHand().indexOf(c) + 1) + ": " + c.hashCode() + ConsoleColors.ANSI_RESET));

    }
}