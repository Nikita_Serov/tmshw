package BlackJack.Cards;

public enum CardSuit {
    HEARTS,
    CLUBS,
    TAMBOURINE,
    PEAK
}
