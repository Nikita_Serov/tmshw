package BlackJack.Cards;

public enum CardValue {
    J(10, "J"),
    Q(10, "Q"),
    K(10, "K"),
    A(11, "A"),
    TWO(2, "2"),
    THREE(3, "3"),
    FOUR(4, "4"),
    FIVE(5, "5"),
    SIX(6, "6"),
    SEVEN(7, "7"),
    EIGHT(8, "8"),
    NINE(9, "9"),
    TEN(10, "10");

    int numberOfPoints;
    public String name;

    CardValue(int numberOfPoints, String name) {
        this.numberOfPoints = numberOfPoints;
        this.name = name;
    }
}
