package BlackJack.Cards;

public class Card {
    private final String suit;
    private final String name;
    private final int points;

    public Card(CardSuit suit, CardValue value) {
        this.suit = suit.toString();
        this.name = value.name;
        this.points = value.numberOfPoints;
    }

    public String getName() {
        return name;
    }

    public int getPoints() {
        return points;
    }

    @Override
    public String toString() {
        return this.suit + " " + this.name
                + " (points:" + this.points + ")";
    }
}
