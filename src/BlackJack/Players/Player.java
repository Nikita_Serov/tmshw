package BlackJack.Players;
import BlackJack.Cards.Card;
import BlackJack.Cards.CardValue;
import BlackJack.ConsoleColors;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Random;

public class Player/*, Comparator<Player>*/ {
    private String name;
    private boolean lose = false;
    private int countOfPoints = 0;
    private boolean enough;
    ArrayList<Card> hand = new ArrayList<>();

    public void turn(ArrayList<Card> deck) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println(this.toString());
        if (!isEnough()) {
            System.out.println("Enter 1 to take card, 2 to end you turn:");
            try {
                String choice = reader.readLine();
                switch (choice) {
                    case "1":
                        this.takeCard(deck);
                        break;
                    case "2":
                        this.enough();
                        break;
                    default: {
                        System.out.println(ConsoleColors.ANSI_RED
                                + "Write correct number (1 or 2)" + ConsoleColors.ANSI_RESET);
                        this.turn(deck);
                    }
                }
            } catch (Exception exception) {
                exception.printStackTrace();
            }
        }
        if (this.countOfPoints == 21)
            this.enough();
    }


    public void takeCard(ArrayList<Card> deck) {
        System.out.println(name);
        Random r = new Random();
        // take the random card
        Card randomCard = deck.get(r.nextInt(deck.size()));
        this.hand.add(randomCard);
        System.out.println(ConsoleColors.ANSI_GREEN
                + "Your take the card: " + randomCard.toString() + ConsoleColors.ANSI_RESET);
        this.setCountOfPoints(randomCard);
        //remove our card from deck
        deck.remove(randomCard);
        System.out.println(this.toString());
    }

    public void setCountOfPoints(Card card) {
        //with special logic for Ace
        if (!card.getName().equals(CardValue.A.name))
            this.countOfPoints += card.getPoints();
        else {
            if (this.getCountOfPoints() <= 10)
                this.countOfPoints += card.getPoints();
            else {
                this.countOfPoints += 1;
                System.out.println(ConsoleColors.ANSI_RED
                        + "Ace increased the value of your points at 1" + ConsoleColors.ANSI_RESET);
            }
        }
        //if points > 21 you immediately lose, sorry
        if (this.countOfPoints > 21) {
            this.lose = true;
            this.enough = true;
            System.out.println(ConsoleColors.ANSI_RED + name
                    + " END OF YOUR TURNS BECAUSE YOU ALREADY LOSE!" + ConsoleColors.ANSI_RESET);
        }
    }

    public boolean isEnough() {
        return enough;
    }

    public void enough() {
        this.enough = true;

        //ok if you count of points is already 21 y can't take card again
        if (this.countOfPoints == 21)
            System.out.println(ConsoleColors.ANSI_RED + this.name
                    + " you are 21, no need to dial yet" + ConsoleColors.ANSI_RESET);

        System.out.println(ConsoleColors.ANSI_YELLOW
                + "It's enough..." + ConsoleColors.ANSI_RESET);
    }

    public Player(String name) {
        this.name = name;
    }

    public Player() {
        this.name = "Noname";
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public boolean isLose() {
        return lose;
    }

    public int getCountOfPoints() {
        return this.countOfPoints;
    }

    public ArrayList<Card> getHand() {
        return hand;
    }

    @Override
    public String toString() {
        return "Your stats: \nYour hand:" + hand.toString()
                + ", total count of points: " + ConsoleColors.ANSI_RED
                +  countOfPoints + ConsoleColors.ANSI_RESET;
    }

    public static int compare(Player p1, Player p2) {
        if (p1.getCountOfPoints() > p2.getCountOfPoints())
            return 1;
        else
            return -1;
    }
}
