package BlackJack.Players;
import BlackJack.Cards.Card;
import BlackJack.Cards.CardSuit;
import BlackJack.Cards.CardValue;
import BlackJack.ConsoleColors;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;


public class Croupier extends Player {
    ArrayList<Card> deck = new ArrayList<>(52);

    @Override
    public void turn(ArrayList<Card> deck) {
        if (this.getCountOfPoints() <= 15) {
            this.takeCard(deck);
        } else if (this.getCountOfPoints() <= 15 && this.getCountOfPoints() <= 20) {
            this.hmmmBeOrNotToBe();
        } else {
            System.out.println(ConsoleColors.ANSI_YELLOW + getName()
                    + " He thinks that he has enough" + ConsoleColors.ANSI_RESET);
            this.enough();
        }
    }


    @Override
    public void takeCard(ArrayList<Card> deck) {
        System.out.println(ConsoleColors.ANSI_GREEN
                + getName() + " take a card" + ConsoleColors.ANSI_RESET);
        Random r = new Random();
        Card randomCard = deck.get(r.nextInt(deck.size()));
        this.hand.add(randomCard);
        this.setCountOfPoints(randomCard);
        deck.remove(randomCard);
        System.out.println(ConsoleColors.ANSI_RED + "Hashcode of this turn ("
                + (hand.indexOf(randomCard) + 1) + "): " + randomCard.hashCode() + ConsoleColors.ANSI_RESET);
    }

    public void fillingTheDeck(ArrayList<Card> deck) {
        //cycle for Suits
        for (CardSuit suit : CardSuit.values()) {
            //cycle for Values
            for (CardValue value : CardValue.values()) {
                deck.add(new Card(suit, value));
            }
        }
    }

    public void hmmmBeOrNotToBe() {
        Random random = new Random();
        int r = random.nextInt(100);
        if (r <= 5) {
            this.takeCard(deck);
        }
    }

    public void shuffleTheDeck(ArrayList<Card> deck) {
        Collections.shuffle(deck);
    }

    public Croupier() {
        this.setName("Croupier");
    }
    public ArrayList<Card> getDeck() {
        return deck;
    }
}

