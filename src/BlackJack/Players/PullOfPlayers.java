package BlackJack.Players;

import BlackJack.ConsoleColors;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class PullOfPlayers {
    private ArrayList<Player> table;

    public void howManyPlayersWantToPlay() {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("How much players will play?");
        try {
            String str = reader.readLine();
            int inpCount = Integer.parseInt(str);
            if ((inpCount >= 1 && inpCount <= 7)) {
                table = new ArrayList<>(inpCount + 1);
                for (int i = 0; i < inpCount; i++) {
                    addPlayer(i);
                }
                System.out.println(ConsoleColors.ANSI_RED + "Ok, we have " + table.size() + " players."
                        + ConsoleColors.ANSI_CYAN + "\nThe game started!" + ConsoleColors.ANSI_RESET);
            } else {
                System.err.println("Input correct count of players (from 1 to 5)");
                howManyPlayersWantToPlay();
            }
        } catch (IOException | NumberFormatException exception) {
            System.err.println("Input correct count of players (from 1 to 5)");
            howManyPlayersWantToPlay();
        }
    }

    public void addPlayer(int id) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("What is your name player #" + (id + 1) + "?");
        try {
            String name = reader.readLine();
            System.out.println("New player " + "\"" + name + "\"");
            Player p = new Player(name);
            table.add(p);
        } catch (IOException exception) {
            exception.printStackTrace();
        }
    }

    public ArrayList<Player> getTable() {
        return table;
    }
}
