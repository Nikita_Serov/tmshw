package HomeWork6;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/*2) Заменить все вхождения символа стоящего в позиции (3) на сивол стоящий в
        позиции 0*/
public class ReplacementAllCharsWith3IndexToCharWith0Index {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String str;

        while (true) {
            System.out.println("______________________________________");
            System.out.println("Input string:");
            str = reader.readLine();
            System.out.println("You entered: " + str);

            if (str.equals("ex"))
                break;
            System.out.println("Char with index 0: " + str.charAt(0));

            try {
                System.out.println("Char with index 3: " + str.charAt(3));
                str = str.replace(str.charAt(3), str.charAt(0));
                System.out.println("String with replace:" + str);
            } catch (StringIndexOutOfBoundsException exception) {
                System.out.println("Short string for index 3, try again");
            }
        }
    }
}
