package HomeWork6.Palindromes;
import java.io.*;


/*3)В исходном файле находятся слова, каждое слово на новой стороке. После
        запуска программы должен создать файл, который будет содержать в себе
        только полиндромы*/
public class Palindromes {

    public static void main(String[] args) {
        fileReader();
    }

    static void fileReader() {
        try (BufferedReader br = new BufferedReader(new FileReader
                ("E:\\7 Java\\TMS\\tmshw\\src\\HomeWork6\\Palindromes\\Words.txt"))) {

            String s;
            StringBuilder stringBuilder = new StringBuilder();

            System.out.println("В исходном файле:");
            while ((s = br.readLine()) != null) {
                System.out.println(s);
                if (isPalindrome(s)) {
                    stringBuilder.append(s).append("\n");
                }
            }

            palindromesWrite(stringBuilder.toString());
            System.out.println("В новый файл записаны:\n" + stringBuilder.toString());
        } catch (IOException exception) {
            System.err.println(exception.getMessage());
        }
    }

    static void palindromesWrite(String palindrome) {
        try (BufferedWriter bw = new BufferedWriter(new FileWriter
                ("E:\\7 Java\\TMS\\tmshw\\src\\HomeWork6\\Palindromes\\Palindromes.txt"))) {
            bw.write(palindrome);
        } catch (IOException exception) {
            System.err.println(exception.getMessage());
        }
    }

    static boolean isPalindrome(String str) {
        StringBuilder sb = new StringBuilder();
        for (int i = str.length(); i > 0; i--) {
            sb.append(str.charAt(i - 1));
        }
        String revStr = sb.toString();
        return revStr.equalsIgnoreCase(str);
    }
}
