package HomeWork6;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/*1) Вырезать подстроку из строки начиная с первого вхождения символа(А) до,
        последнего вхождения сивола(B).*/
public class StringCut {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String str;

        while (true) {
            System.out.println("Input string with A & B symbols:");
            str = reader.readLine();
            System.out.println("You entered: " + str);

            if (str.equals("ex"))
                break;

            if (str.indexOf('A') != -1 & str.indexOf('B') != -1) {
                System.out.println("First index of A: " + str.indexOf('A'));
                System.out.println("Last index of B: " + str.lastIndexOf('B'));
                System.out.println("Substring between A & B: " +
                        str.substring(str.indexOf('A'), str.lastIndexOf('B') + 1));
            } else
                System.out.println("I can't find A & B symbols");

        }
    }
}
