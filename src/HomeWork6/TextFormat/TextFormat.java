package HomeWork6.TextFormat;
import java.io.*;
/*    4)Текстовый файл содержит текст. После запуска программы в другой файл
    должны записаться только те предложения в которых от 3-х до 5-ти слов. Если в
    предложении присутствует слово-палиндром, то не имеет значения какое кол-во
    слов в предложении, оно попадает в новый файл.
    Пишем все в ООП стиле. Создаём класс TextFormater
    в котором два статических метода
     Разбиваем текст на предложения. Используя методы класса TextFormater
    определяем подходит ли нам предложение. Если подходит добавляем его в
    новый файл */
public class TextFormat {
    public static void main(String[] args) {

        StringBuilder stringBuilder = new StringBuilder();

        try (BufferedReader br = new BufferedReader(new FileReader
                ("E:\\7 Java\\TMS\\tmshw\\src\\HomeWork6\\TextFormat\\Story.txt"))) {
            String s;

            while ((s = br.readLine()) != null) {
                if (isPalindrome(s) | (countOfWords(s) >= 3 && countOfWords(s) <= 5))
                    stringBuilder.append(s).append("\n");
            }
        } catch (IOException exception) {
            System.err.println(exception.getMessage());
        }

        try (BufferedWriter bw = new BufferedWriter(new FileWriter
                ("E:\\7 Java\\TMS\\tmshw\\src\\HomeWork6\\TextFormat\\AnotherStory.txt"))) {
            bw.write(stringBuilder.toString());
        } catch (IOException exception) {
            System.err.println(exception.getMessage());
        }
    }

    static char countOfWords(String s) {

        char count = 0;

        if (s.length() != 0) {
            count++;
            for (int i = 0; i < s.length(); i++) {
                if (s.charAt(i) == ' ') {
                    count++;
                }
            }
        }
        return count;
    }


    static boolean isPalindrome(String s) {
        StringBuilder si = new StringBuilder();

        if (s.length() != 0) {
            for (int i = 0; i < s.length(); i++) {
                if (s.charAt(i) != ' ' & s.charAt(i) != '.' & s.charAt(i) != '!' & s.charAt(i) != '?') {
                    si.append(s.charAt(i));
                } else {

                    StringBuilder sk = new StringBuilder();

                    for (int k = si.length(); k > 0; k--) {
                        sk.append(si.charAt(k - 1));
                    }

                    if (sk.toString().equalsIgnoreCase(si.toString())) {
                        return true;
                    }

                    si = new StringBuilder();
                }
            }
        }
        return false;
    }
}
