package HomeWork9;

import java.util.*;

public class Shop {
    ArrayList<Product> storage = new ArrayList<>();

    public void addProduct(Product product) {
        //Does such a product exist in the collection
        boolean isExistInStorage = false;
        for (Product p :
                storage) {
            if (p.getId() == product.getId()) {
                isExistInStorage = true;
                System.out.println("Product " + product.toString()
                        + " not added (this id already exist)");
                break;
            }
        }
        if (!isExistInStorage) {
            storage.add(product);
            product.setTimeOfAdd();
            System.out.println("Product " + product.toString() + " has been added");
        }
    }

    public ArrayList<Product> getAllProducts() {
        return storage;
    }

    public void removeProduct(Integer id) {
        //Does such a product exist in the collection
        boolean isExistInStorage = false;
        int indexOfRemovable = 0;
        for (Product p :
                storage) {
            if (p.getId() == id) {
                isExistInStorage = true;
                System.out.println("Product " + p.toString() + " has been removed");
                indexOfRemovable = storage.indexOf(p);
            }
        }
        if (!isExistInStorage) {
            System.out.println("Product with this id not exist");
        } else
            storage.remove(indexOfRemovable);
    }

    public void changeProduct(Product product) {
        int indexOfInputProduct = storage.indexOf(product);
        if (indexOfInputProduct != -1) {
            System.out.println("Product " + storage.get(indexOfInputProduct).toString()
                    + " has been changed to ");
            product.setName("Action coupon");
            product.setId(404);
            product.setPrice(0);
            System.out.println(storage.get(indexOfInputProduct).toString());
        } else
            System.out.println("This product "
                    + product.toString() + " not exist in shop");
    }
}

