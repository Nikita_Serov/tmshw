package HomeWork9;

import java.util.Comparator;

public class Main {
    public static void main(String[] args) {
        Shop magazine = new Shop();
        Product cocaCola = new Product(12, "CocaCola", 15);
        Product snickers = new Product(14, "Snickers", 25);
        Product cake = new Product(5, "Cake", 10);
        Product butter = new Product(12, "Butter", 20);
        Product cookie = new Product(6, "Cookie", 5);
        Product sandwich = new Product(11, "Sandwich", 20);

        //add products
        magazine.addProduct(cocaCola);
        magazine.addProduct(snickers);
        magazine.addProduct(cake);
        magazine.addProduct(butter); //this product not will be added because this id is already exist
        magazine.addProduct(cookie);

        //list output
        System.out.println("List of products: \n" + magazine.getAllProducts());


        //list sorting by comparator (price)
        Comparator<Product> compareByPrice = Comparator.comparingInt(Product::getPrice); //try to use func.interface
        magazine.getAllProducts().sort(compareByPrice);
        System.out.println("Sort by price: \n" + magazine.getAllProducts());

        //remove element from list
        magazine.removeProduct(5);

        /*i will add another one product to check his time of add
         (i want make sure, what this   will not change another products time)*/
        magazine.addProduct(sandwich);

        //list output
        System.out.println("List of products: \n" + magazine.getAllProducts());

        //list sorting by time of adding
        Comparator<Product> compareByTime = (o1, o2) -> o2.getTimeOfAdd() - o1.getTimeOfAdd();
        magazine.getAllProducts().sort(compareByTime);
        System.out.println("Sort by time: \n" + magazine.getAllProducts());

        //editing of the product from list
        magazine.changeProduct(cocaCola);

        //list output
        System.out.println(magazine.getAllProducts());
    }
}
