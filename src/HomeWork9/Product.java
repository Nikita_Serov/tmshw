package HomeWork9;



public class Product{
    private int id;
    private String name;
    private int price;
    /* if i will initiate static field (staticTimeOfAdd - is the counter),
       i can initiate personal time for each unique product with setter: */
    private static int staticTimeOfAdd = 1;
    private int productTimeOfAdd;

    public Product(int id, String name, int price) {
        this.id = id;
        this.name = name;
        this.price = price;
    }

    public int getTimeOfAdd() {
        return productTimeOfAdd;
    }

    public void setTimeOfAdd() {

        this.productTimeOfAdd = staticTimeOfAdd;
        staticTimeOfAdd+=1;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return name + " (id:" + id + ", price:" + price  + " timeOfAdd:" + productTimeOfAdd+ ")";
    }
}
