package HomeWork7;

public class Run {
    public static void main(String[] args) {
        Car audi = new Car("Audi", 300.0, 100000);
        Car honda = new Car("Honda", 230, 20000);
        Car tesla = new Car("Tesla", 280, 35000);
        Car jigul = new Car("Jigu", 150, 3000);

        Car[] cars = {audi,honda,tesla, jigul};
        for (Car car:cars) {
            try {
                car.start();

            } catch (CustomException exception) {
                System.err.println(exception.getMessage());
            }
        }
    }
}
