package HomeWork7;

import java.util.Random;

/*1) Создать собственное исключение
        - Создать класс Car c полями (марка, скорость, цена), конструкторы (с
        параметрами и default) гетеры-сетеры.

        Создать метод старт в котором пытаетесь завести автомобиль. В методе старт
        генерируете случайное число от 0 до 20, если полученное число оказалось
        четным, то выбрасываете созданное ранее вами исключение и передаете его к
        месту откуда вызывали метод старт. Если все хорошо и исключение не
        вылетело, то выводить в консоль сообщение что автомобиль с такой-то маркой
        завелся.*/
public class Car {
    private String mark;
    private double speed;
    private int cost;

    public Car(String mark, double speed, int cost) {
        this.mark = mark;
        this.speed = speed;
        this.cost = cost;
    }

    void start() throws CustomException {
        Random r = new Random();
        int random = r.nextInt(20);

        if (random % 2 != 0) {
            System.out.println(mark + " started up!");
        } else throw new CustomException(mark + " start is falled!");
    }

    public String getMark() {
        return mark;
    }

    public void setMark(String mark) {
        this.mark = mark;
    }

    public double getSpeed() {
        return speed;
    }

    public void setSpeed(double speed) {
        this.speed = speed;
    }

    public double getCost() {
        return cost;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }
}
