package HomeWork2;
/*5)Напишите программу печати таблицы перевода расстояний из дюймов в
        сантиметры для значений длин от 1 до 20 дюймов. 1 дюйм = 2,54 см*/
public class Task25 {
    public static void main(String[] args) {
        double d = 1, sm, diff = 2.54;
        double[] dym = new double[21];
        double[] san = new double[21];

        System.out.println("| Дюйм   | Сантиметр |");
        System.out.println("----------------------");
        for (int i = 1; i<21; i++) {
            dym[i]=i;
            san[i]=dym[i]*diff;

            if(i<10)
            System.out.println("|" + dym[i] + "     | " + san[i]);
            else
                System.out.println("|" + dym[i] + "    | " + san[i]);
            System.out.println("----------------------");
        }


    }


}
