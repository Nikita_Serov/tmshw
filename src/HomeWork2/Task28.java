package HomeWork2;
import java.io.BufferedReader;
import java.io.InputStreamReader;
/*
8) И ещё можете попрактиковаться и нарисовать оставшиеся 2
        треугольника
        *
        * *       (1)
        * * *
        * * * *

        * * * *
        * * *     (2)
        * *
        *
*/

public class Task28 {
    public static void main(String[] args){
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Введите максимальное количество символов в строке: ");
       try {
           String s = reader.readLine();
           int j = Integer.parseInt(s);


           for (int i = 1; i < (j + 1); i++) {
               for (int k = 0; k < (i); k++) {
                   System.out.print("* ");
               }
               System.out.println("");
           }
           System.out.print("\n");
           for (int i = 1; i < (j + 1); i++) {
               for (int k = i; k < (j + 1); k++) {
                   System.out.print("* ");
               }
               System.out.println("");
           }
       }catch (Exception e) {System.out.println("Введено недопустимое значение");}
        System.out.println("Завершение программы");

    }
}
