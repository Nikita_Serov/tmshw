package HomeWork2;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/*Начав тренировки, спортсмен в первый день пробежал 10 км. Каждый
        день он увеличивал дневную норму на 10% нормы предыдущего дня. Какой
        суммарный путь пробежит спортсмен за 7 дней?*/

public class Task21 {
    public static void main(String[] args) throws IOException {
        double n = 0.1;
        double s = 10;
        double sum =0;
        System.out.println("Пройденное расстояние за 1 день: " + s);
            for (int i = 1; i < 7; i++) {
              System.out.println("Day " + (i+1));
                s = s + (s*n);
                System.out.println("Пройденное расстояние за " + (i+1) + " день: " + s);
                sum += s;
            }
        System.out.println("Всего пробежал за 7 дней: " + sum);
        System.out.println("Terminate");
        }
    }

