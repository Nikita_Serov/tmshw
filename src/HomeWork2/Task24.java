package HomeWork2;



import java.io.BufferedReader;

import java.io.InputStreamReader;

//4)Составьте программу, вычисляющую A*B, не пользуясь операцией умножения.

public class Task24 {
    public static void main(String[] args){
        int a, b; //объявляем переменные для условных чисел
        // double a, b; //объявляем переменные для условных чисел
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        try {
        while(true) { //бесконечный цикл чтобы непрервыно вводить данные и не перезапускать класс

            int mult = 0;
           // double mult = 0; //переменная-счетчик для произведения 2 чисел

            System.out.println("-----------------------");
            System.out.println("Введите число А: ");
            String n1 = reader.readLine(); //считываем строчные данные с клавиатуры для числа А
            if (n1.equals("ex")) break;  //если при считывании числа А пользователь вводит ех - выход из бесконечного цикла
            System.out.println("Введите число В: ");
            String n2 = reader.readLine(); //считываем строчные данные с клавиатуры для числа В
            if (n2.equals("ex")) break; //если при считывании числа В пользователь вводит ех - выход из бесконечного цикла

            a = Integer.parseInt(n1);
            b = Integer.parseInt(n2);
           // a = Double.parseDouble(n1); //приводим строку числа А в тип int c помощью класса обертки, присваиваем ее полю а
           // b = Double.parseDouble(n2); //приводим строку числа B в тип int c помощью класса обертки присваиваем ее полю в


            for (int i = 1; i <= a; i++){ // цикл повторяется в количестве "а" раз
                /*Умножение определяется как многократное сложение —
                чтобы умножить число a на число b надо сложить b чисел a раз*/

                mult +=b; //каждую итерацию переменная счетчик увеличивается на число В
                System.out.println(i + " итерация: " + mult);
            }
            System.out.println("Произведение А на В: " + mult);
        }}catch (Exception e){
            System.out.println("Введен не верный формат");
        }
        System.out.println("Программа завершена...");
    }
}
