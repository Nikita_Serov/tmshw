package HomeWork8.Serialization;
import java.io.Serializable;
import java.util.Random;
public class Car implements Serializable {
    private String mark;
    private double speed;
    private int cost;

    public Car(String mark, double speed, int cost) {
        this.mark = mark;
        this.speed = speed;
        this.cost = cost;
    }

    @Override
    public String toString() {
        return "Mark: " + this.mark + " Speed: " + this.speed + " Cost: " + this.cost ;
    }

    void start() throws CustomException {
        Random r = new Random();
        int random = r.nextInt(20);

        if (random % 2 != 0) {
            System.out.println(mark + " started up!");
        } else throw new CustomException(mark + " start is falled!");
    }

    public String getMark() {
        return mark;
    }

    public void setMark(String mark) {
        this.mark = mark;
    }

    public double getSpeed() {
        return speed;
    }

    public void setSpeed(double speed) {
        this.speed = speed;
    }

    public double getCost() {
        return cost;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }
}
