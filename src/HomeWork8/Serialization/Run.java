package HomeWork8.Serialization;


import java.io.*;

public class Run {
    public static void main(String[] args) {
        Car audi = new Car("Audi", 300.0, 100000);
        Car honda = new Car("Honda", 230, 20000);
        Car tesla = new Car("Tesla", 280, 35000);
        Car jigul = new Car("Jiguli", 150, 3000);

        Car[] cars = {audi, honda, tesla, jigul};
        for (Car car : cars) {
            try {
                car.start();

            } catch (CustomException exception) {
                System.err.println(exception.getMessage());
            }
        }

        serWrite(audi);

        serRead();

    }

    public static void serWrite(Car car) {
        try (FileOutputStream fos = new FileOutputStream(
                "E:\\7 Java\\TMS\\tmshw\\src\\HomeWork8\\Serialization\\SerializableObject.txt");
             ObjectOutputStream oos = new ObjectOutputStream(fos)) {
            System.out.println("Serialize " + car.getMark());
            oos.writeObject(car);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void serRead() {
        try (FileInputStream fis = new FileInputStream(
                "E:\\7 Java\\TMS\\tmshw\\src\\HomeWork8\\Serialization\\SerializableObject.txt");
             ObjectInputStream ois = new ObjectInputStream(fis)) {
            Car serializableCar = (Car) ois.readObject();
            System.out.println("Saved car after deserialization: CarName: " + serializableCar.getMark()
                    + "\n CarCost: " + serializableCar.getCost()
                    + "\n CarSpeed: " + serializableCar.getSpeed());
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
