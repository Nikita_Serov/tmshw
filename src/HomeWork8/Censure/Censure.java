package HomeWork8.Censure;

import java.io.*;
import java.util.ArrayList;

/*Создаете 2 файла.
        1 - й. Содержит исходный текст.
        2 - й. Содержит слова недопустимые в тексте(black list). Структура файла
        определите сами, хотите каждое слово на новой строке, хотите через запятую
        разделяйте, ваша программа делайте как считаете нужным.
        Задача: необходимо проверить проходит ли текст цензуру. Если в тексте не
        встретилось ни одного недопустимого слова, то выводите сообщение о том что
        текст прошёл проверку на цензуру. Если нет, то выводите соответствуюущее
        сообщение, кол-во предложений не прошедших проверку и сами предложения
        подлежащие исправлению.*/
public class Censure {
    public static void main(String[] args) {
        ArrayList<String> badWords = new ArrayList<>();
        final String ANSI_RED = "\u001B[31m";
        final String ANSI_RESET = "\u001B[0m";

        try (BufferedReader br = new BufferedReader(new FileReader
                ("E:\\7 Java\\TMS\\tmshw\\src\\HomeWork8\\Censure\\BadWords.txt"))) {

            String badword;

            while ((badword = br.readLine()) != null) {
                badWords.add(badword);
            }

        } catch (IOException exception) {
            System.err.println(exception.getMessage());
        }
        System.out.println("List of forbidden words: " + ANSI_RED + badWords.toString() + ANSI_RESET);

        try (BufferedReader br = new BufferedReader(new FileReader
                ("E:\\7 Java\\TMS\\tmshw\\src\\HomeWork8\\Censure\\Text.txt"))) {

            String str;
            int countOfbad = 0;

            while ((str = br.readLine()) != null) {
                boolean isbad = isBadWord(str, badWords);
                if (isbad) {
                    countOfbad++;
                    if (countOfbad == 1) {
                        System.out.println("___________________________" +
                                "\n" + ANSI_RED + "THE TEXT DID NOT PASS CENSORSHIP CHECK!" + ANSI_RESET);
                    }
                    System.out.println("Forbidden word in sentence #" + (countOfbad) + ":");
                    System.out.println(ANSI_RED + str + ANSI_RESET);
                }
            }

            if (countOfbad != 0)
                System.out.println("___________________________" +
                        "\n" + "Count of sentences with forbidden words: " + countOfbad);
            else
                System.out.println("Text is approved.");

        } catch (IOException exception) {
            System.err.println(exception.getMessage());
        }
    }


    static boolean isBadWord(String s, ArrayList<String> badwords) {
        StringBuilder sentenceBuilder = new StringBuilder();
        if (s.length() != 0) {
            for (int i = 0; i < s.length(); i++) {

                if (s.charAt(i) != ' ' & s.charAt(i) != '.'
                        & s.charAt(i) != '!' & s.charAt(i) != '?' & s.charAt(i) != ':') {
                    sentenceBuilder.append(s.toLowerCase().charAt(i));
                } else {
                    StringBuilder wordBuilder = new StringBuilder();

                    for (int k = 0; k < sentenceBuilder.length(); k++) {
                        if (s.charAt(i) == ' ' & s.charAt(i) == '.' &
                                s.charAt(i) == '!' & s.charAt(i) == '?' & s.charAt(i) == ':')
                            break;
                        else {
                            wordBuilder.append(sentenceBuilder.charAt(k));
                        }
                    }

                    if (badwords.contains(wordBuilder.toString())) {
                        return true;
                    }
                    sentenceBuilder = new StringBuilder(); //
                }
            }
        }
        return false;
    }
}
